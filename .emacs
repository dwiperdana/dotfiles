(package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes (quote (wheatgrass)))
 '(org-agenda-files (quote ("~/Dropbox/dayjournal/FY2018-FQ01-Apr-Jun.org")))
 '(org-default-priority 99)
 '(package-selected-packages (quote (ox-hugo elpy ace-window))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'package)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/"))
(require 'package)
(add-to-list 'package-archives
             '("elpy" . "https://jorgenschaefer.github.io/packages/"))

;;org-mode settings
(require 'org)
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)
(setq org-startup-indented t)
(setq org-hide-leading-stars t)
(setq org-completion-use-ido t)
(setq org-indent-mode t)


(add-to-list 'load-path "~/.emacs.d/lisp")

;; set up ido mode
(require `ido)
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)

;; set up fonts for different OSes. OSX toggles to full screen.
(setq myfont "Meslo LG M DZ for PowerLine")
(cond
((string-equal system-name "w0rk")
 (set-face-attribute 'default nil :font myfont :height 90)
 ;;(toggle-frame-fullscreen)
 )
((string-equal system-name "phobos")
 (set-face-attribute 'default nil :font myfont :height 90)
 ;;(toggle-frame-fullscreen)
 )
)

(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode 1))

(setq org-cycle-emulate-tab 'white)

(global-set-key (kbd "C-<tab>") 'hide-subtree)
(global-visual-line-mode t)


;; (elpy-enable)

;; (global-set-key (kbd "M-p") 'ace-window)
(global-set-key (kbd "C-.") #'other-window)
(global-set-key (kbd "C-,") #'prev-window)

(defun prev-window ()
  (interactive)
  (other-window -1)
)

;; load emacs 24's package system. Add MELPA repository.
(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   ;; '("melpa" . "http://stable.melpa.org/packages/") ; many packages won't show if using stable
   '("melpa" . "http://melpa.milkbox.net/packages/")
   t))

;;(with-eval-after-load 'ox
;;  (require 'ox-hugo))
