#!/usr/bin/env bash

# use the bash as default "sh", fixed some problems
# with e.g. third-party scripts
#sudo ln -sf /bin/bash /bin/sh

`
Notes:
1. Install berbagai software
2. Copy file dotfiles ke root

List dotfiles:
1. .xbindkeysrc
2. .zshrc
3. .aliases
4. .gitconfig
5. .gitignore
6. .emacsrc
7. fresh-install.sh
8. bootstrap.sh

`


function ask_install() {
  echo
  echo
  read -p"$1 (y/n) " -n 1
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    return 1
  else
    return 0
  fi

}

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
  echo "This script must be run as root"
  echo "Plese use sudo or su"
  exit 1
fi

# use aptitude in the next steps ...
if [ \! -f $(whereis aptitude | cut -f 2 -d ' ') ] ; then
  apt-get install aptitude
fi

# update && upgrade
ask_install "upgrade your system?"
if [[ $? -eq 1 ]]; then
  aptitude update
  aptitude upgrade
fi


aptitude install \
  `# read-write NTFS driver for Linux` \
  ntfs-3g \
  `# do not delete main-system-dirs` \
  safe-rm \
  `# default for many other things` \
  tmux \
  build-essential \
  autoconf \
  make \
  cmake \
  mktemp \
  dialog \
  `# unzip, unrar etc.` \
  cabextract \
  zip \
  unzip \
  rar \
  unrar \
  tar \
  pigz \
  p7zip \
  p7zip-full \
  p7zip-rar \
  unace \
  bzip2 \
  gzip \
  xz-utils \
  advancecomp \
  `# optimize image-size` \
  gifsicle \
  optipng \
  pngcrush \
  pngnq \
  pngquant \
  jpegoptim \
  libjpeg-progs \
  jhead \
  `# utilities` \
  coreutils  \
  findutils  \
  `# fast alternative to dpkg -L and dpkg -S` \
  dlocate \
  `# quickly find files on the filesystem based on their name` \
  mlocate \
  locales \
  `# removing unneeded localizations` \
  localepurge \
  sysstat \
  tcpdump \
  colordiff \
  moreutils \
  atop \
  ack-grep \
  ngrep \
  `# interactive processes viewer` \
  htop \
  `# mysql processes viewer` \
  mytop \
  `# interactive I/O viewer` \
  iotop \
  tree \
  `# disk usage viewer` \
  ncdu \
  rsync \
  whois \
  vim \
  csstidy \
  recode \
  exuberant-ctags \
  `# GNU bash` \
  bash \
  bash-completion \
  `# command line clipboard` \
  xclip \
  `# more colors in the shell` \
  grc \
  `# fonts also "non-free"-fonts` \
  `# -- you need "multiverse" || "non-free" sources in your "source.list" -- ` \
  fontconfig \
  ttf-freefont \
  ttf-mscorefonts-installer \
  ttf-bitstream-vera \
  ttf-dejavu \
  ttf-liberation \
  ttf-linux-libertine \
  ttf-larabie-deco \
  ttf-larabie-straight \
  ttf-larabie-uncommon \
  ttf-liberation \
  xfonts-jmk \
  `# trace everything` \
  strace \
  `# get files from web` \
  wget \
  curl \
  w3m \
  aria2 \
  `# repo-tools`\
  git \
  `#subversion` \
  `#mercurial` \
  `# usefull tools` \
  nodejs \
  npm \
  ruby-full \
  imagemagick \
  lynx \
  nmap \
  pv \
  ucspi-tcp \
  xpdf \
  sqlite3 \
  perl \
  python \
  python-pip \
  python-dev \
  `# install python-pygments for json print` \
  python-pygments \
  firefox \
  terminator \
  dropbox \
  shutter \
  gimp \
  `#inkscape` \
  `#krita` \
  `#obs-studio` \
  `#terminator` \
  `#spotify-client` \
  `#libreoffice`
  

# try zsh?
read -p "Do you want to use the zsh-shell? (y/n) " -n 1 yesOrNo
echo
if [[ $yesOrNo =~ ^[Yy]$ ]]; then
  sudo aptitude install zsh
  chsh -s $(which zsh)
fi

#
# fixing nodejs for ubuntu
#
ln -s /usr/bin/nodejs /usr/bin/node

#
# install new git / ubuntu
#

sudo add-apt-repository -y ppa:git-core/ppa
sudo aptitude update
sudo aptitude upgrade git

#### DP INSTALL ##########################################################

#
# TODO install emacs (and setup extensions)
#

#
# TODO install i3wm, i3blocks
#


#### END OF DP INSTALL ###################################################

# clean downloaded and already installed packages
aptitude -v clean

# update-fonts
cp -vr $( dirname "${BASH_SOURCE[0]}" )/.fonts/* /usr/share/fonts/truetype/
dpkg-reconfigure fontconfig
fc-cache -fv

# update-locate-db
echo "update-locate-db ..."
updatedb -v
