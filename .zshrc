# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
  export ZSH=/home/dp/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="agnoster"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

alias up="sudo apt-get update && sudo apt-get upgrade -y"

#DEFAULT_USER=`whoami`

#alias emacs='XLIB_SKIP_ARGB_VISUALS=1 emacs'

DEFAULT_USER="dp"

# alias n="neofetch"
alias up="sudo apt-get update && sudo apt-get upgrade -y"
alias c="clear"

alias hdmileft="xrandr --output HDMI1 --auto --left-of LVDS1"
alias hdmiright="xrandr --output HDMI1 --auto --right-of LVDS1"
alias hdmiup="xrandr --output HDMI1 --auto --above LVDS1"
alias hdmioff="xrandr --output HDMI1 --off"
alias hdmiclone="xrandr --output LVDS1 --mode 1366x768 --scale 1x1 --output HDMI1 --same-as LVDS1 --mode 1920x1080 --scale 0.711x0.711"

alias vgaleft="xrandr --output VGA1 --auto --left-of LVDS1"
alias vgaright="xrandr --output VGA1 --auto --right-of LVDS1"
alias vgaup="xrandr --output VGA1 --auto --above LVDS1"
alias vgaoff="xrandr --output VGA1 --off"
alias vgaclone="xrandr --output LVDS1 --mode 1366x768 --scale 1x1 --output VGA1 --same-as LVDS1 --mode 1920x1080 --scale 0.711x0.711"

alias hdmitv="xrandr --output HDMI1 --mode 1360x768 --right-of LVDS1"

#export NVM_DIR="/home/dwiperdana/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

alias ph="python -m SimpleHTTPServer"
#alias python=python3.6

alias cn="clear && neofetch"
alias s="clear && sensors | grep Core"

#alias dock="xrandr --output LVDS1 --off && xrandr --output HDMI2 --auto && xrandr --output HDMI3 --auto --left-of HDMI2"
#alias undock="xrandr --output HDMI2 --off && xrandr --output HDMI3 --off && xrandr --output LVDS1 --auto"

alias dock="xrandr --output HDMI2 --auto --above LVDS1"
alias undock="xrandr --output HDMI2 --off"

alias bat="upower -i /org/freedesktop/UPower/devices/battery_BAT0"
alias batstat="sudo tlp-stat --battery"

alias proxyon="sudo systemctl start dnscrypt-proxy"
alias proxyoff="sudo systemctl stop dnscrypt-proxy.socket"

alias cleartrash="rm -rf ~/.local/share/Trash/*"
alias mute="amixer set Master mute"
alias unmute="amixer set Master unmute && amixer sset Headphone on"
alias volumeup="amixer set Master 10%+"
alias volumedown="amixer set Master 10%-"

alias setbg="feh --bg-scale ~/bg.jpg"

#alias python=python3
